import { Component, Inject, OnInit } from '@angular/core';
import { TeamRestService } from '../../rest/team/team.rest.service';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss'],
})
export class PageHomeComponent { }

import { MemberCard, MemberCardsBlock } from '../../rest/team/team.model';
import { SpecialistCard } from './components/specialist-card/specialist-card.model';

export const specialistAdapter = (memberCard: MemberCard ): SpecialistCard => {
  const {title, description, link, text} = memberCard.block;
  return {
    name: title,
    position: description,
    email: link,
    phoneNumber: text,
    imageUrl: memberCard.imageUrl.w400
  };
};

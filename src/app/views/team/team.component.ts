import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { TeamRestService } from '../../rest/team/team.rest.service';
import { specialistAdapter } from './specialist-card.adapter';
import { SpecialistCard } from './components/specialist-card/specialist-card.model';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
  providers: [TeamRestService],
})
export class TeamComponent implements OnInit {
  title$: Observable<string>;
  specialistsCards$: Observable<SpecialistCard[]>;
  constructor(private teamRest: TeamRestService) { }

  ngOnInit(): void {
    const team$ = this.teamRest
      .getTeamMembers()
      .pipe(share());

    this.title$ = team$
      .pipe(map((team) => team.title));
    this.specialistsCards$ = team$
      .pipe(map(({ memberCards }) => Object.values(memberCards).map(specialistAdapter)));
  }
}

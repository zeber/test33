export interface SpecialistCard {
  name: string;
  position: string;
  email: string;
  phoneNumber: string;
  imageUrl: string;
}

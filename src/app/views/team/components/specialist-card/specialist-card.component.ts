import { Component, Input } from '@angular/core';
import { SpecialistCard } from './specialist-card.model';

@Component({
  selector: 'app-specialist-card',
  templateUrl: './specialist-card.component.html',
  styleUrls: ['./specialist-card.component.scss']
})
export class SpecialistCardComponent {
  @Input() card: SpecialistCard;
}
